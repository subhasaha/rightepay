import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
//import { CreateLeadPage } from '../create-lead/create-lead';

@NgModule({
  declarations: [
    HomePage,
   // CreateLeadPage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    //IonicPageModule.forChild(CreateLeadPage)
  ],
})
export class HomePageModule {}
