export class Codes{

    public  EM_INVALID_MOBILE_NUMBER="Invalid mobile number";
    public  EM_INVALID_EMAILID="Invalid email id";
    public  EM_INVALID_PASSWORD="Invalid password";
    public  EM_INVALID_VERIFICATION_ID="Invalid verification id";

    public API_ERROR="500";

    public LSK_IPADDRESS="ipaddress";
    public LSK_USERNAME="username";
    public LSK_PASSWORD="password";
    
    public LSK_USER_INFO_PREFERENCES="user_info_preferences";

    public API_ENDPOINT="https://cors-anywhere.herokuapp.com/http://";
    public API_AUTHENTICATE_USER="rightepay.in/rest/userLogin";
    public API_SEARCH_LEAD=":10080/24online/service/SalesService/searchLead";
    public API_SEARCH_SERVICE=":10080/24online/service/SalesService/searchService";
    public API_CREATE_LEAD=":10080/24online/service/SalesService/createLead";
    public API_GET_LEAD_DETAILS=":10080/24online/service/SalesService/searchLeadTicketwithDetail";
    public API_GET_SERVICE_DETAILS=":10080/24online/service/SalesService/searchServiceTicketwithDetail";
    public API_UPDATE_SERVICE_REQUEST=":10080/24online/service/SalesService/updateServiceTicket";

}