webpackJsonp([2],{

/***/ 685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(696);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_Codes__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_data_data__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_message_helper__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_DataValidation__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, dataValidation, msgHelper, http, codes) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataValidation = dataValidation;
        this.msgHelper = msgHelper;
        this.http = http;
        this.codes = codes;
        //Set to default
        this.ipAddress = "123.201.20.60";
        this.userName = "avijit.ghosh";
        this.password = "avijit.ghosh";
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    RegisterPage.prototype.login = function () {
        var _this = this;
        //Validate the ip address
        if (!this.dataValidation.isValidIpAddress(this.ipAddress)) {
            this.msgHelper.showErrorDialog('Alert!', 'Invalid Ip Address');
            return;
        }
        //Insert the request into the localstorage
        localStorage.setItem(this.codes.LSK_USERNAME, this.userName);
        localStorage.setItem(this.codes.LSK_PASSWORD, this.password);
        //Create the request json
        var loginRequestJson = {
            "username": this.userName,
            "Password": this.password
        };
        var loading = this.msgHelper.showWorkingDialog('Authenticating User');
        localStorage.setItem(this.codes.LSK_IPADDRESS, this.ipAddress);
        this.http.callApi(loginRequestJson, this.codes.API_AUTHENTICATE_USER).then(function (responseJson) {
            loading.dismiss();
            if (_this.dataValidation.isEmptyJson(responseJson)) {
                _this.msgHelper.showErrorDialog('Server error', 'Empty response received from back end server.Please try after some time.');
                return;
            }
            localStorage.setItem(_this.codes.LSK_USER_INFO_PREFERENCES, JSON.stringify(responseJson));
            _this.msgHelper.showToast('Login Successfull !!!');
            _this.navCtrl.setRoot('HomePage');
        }, function (error) {
            loading.dismiss();
            console.error(error);
            _this.msgHelper.showErrorDialog('Server error', error);
        });
        // this.navCtrl.setRoot(HomePage);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"E:\24Online\Code\RightEpay\src\pages\register\register.html"*/'<ion-header>\n\n  <p class="small-text pl-20 pr-20">\n\n    <img src="../../assets/imgs/logo.png" style="width: 70px !important;" />\n\n\n\n    <span style="float: right !important;">\n\n      <ion-icon name="notifications" style="margin-top: 10px !important; color: #eee !important;"></ion-icon>\n\n    </span>\n\n  </p>\n\n</ion-header>\n\n\n\n<ion-content padding class="background">\n\n\n\n  <p class="image-middle-card nomargin">\n\n\n\n  </p>\n\n  <ion-card id="content">\n\n    <ion-avatar id="user-info">\n\n      <img id="user-image" src="../../assets/imgs/user.png" />\n\n    </ion-avatar>\n\n    <ion-card-content>\n\n\n\n      \n\n\n\n\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>Name</ion-label>\n\n        <ion-input type="text" placeholder="Your Name" class="input-underline" [(ngModel)]="name">\n\n        </ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>Pan No</ion-label>\n\n        <ion-input type="text" placeholder="Your Pan No" class="input-underline" [(ngModel)]="panno">\n\n\n\n        </ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>Phone No</ion-label>\n\n        <ion-input type="text" placeholder="Your Phone No" class="input-underline" [(ngModel)]="phone">\n\n        </ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>Email</ion-label>\n\n        <ion-input type="email" placeholder="Your Email" class="input-underline" [(ngModel)]="email">\n\n        </ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>State</ion-label>\n\n        <ion-input type="text" placeholder="Your State" class="input-underline" [(ngModel)]="state">\n\n        </ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label color="primary" stacked>Sponsor ID</ion-label>\n\n        <ion-input type="text" placeholder="Your Sponsor" class="input-underline" [(ngModel)]="sponsor">\n\n        </ion-input>\n\n      </ion-item>\n\n\n\n    </ion-card-content>\n\n    <p style="text-align: right !important; margin-right: 20px !important;">\n\n      <button ion-button clear >Register</button>\n\n    </p>\n\n    <p style="font-size:10px !important">\n\n      <a ion-button clear href="/#/login">Login?</a>\n\n    </p>\n\n  </ion-card>\n\n\n\n\n\n\n\n</ion-content>\n\n\n\n<ion-footer>\n\n\n\n  <p class="pl-20 pr-20 nomargin">\n\n    <span style="float: left !important;"><button ion-button clear>Terms and Conditions</button></span>\n\n    <span\n\n      style="float: right !important; font-size: 10px !important; color: rgb(0, 0, 0) !important; margin-top: 18px !important;">Copyright\n\n      &copy;2020 RightEPay</span>\n\n  </p>\n\n\n\n</ion-footer>'/*ion-inline-end:"E:\24Online\Code\RightEpay\src\pages\register\register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__Utils_DataValidation__["a" /* DataValidation */], __WEBPACK_IMPORTED_MODULE_2__providers_message_helper__["a" /* MessageHelper */],
            __WEBPACK_IMPORTED_MODULE_1__providers_data_data__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_0__Utils_Codes__["a" /* Codes */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=2.js.map