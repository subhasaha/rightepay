webpackJsonp([4],{

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(694);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_Codes__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_data_data__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_message_helper__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Utils_DataValidation__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, dataValidation, msgHelper, http, codes) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataValidation = dataValidation;
        this.msgHelper = msgHelper;
        this.http = http;
        this.codes = codes;
        //Set to default
        this.ipAddress = "123.201.20.60";
        this.userName = "9331989434";
        this.password = "9567";
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.ngOnInit = function () {
        this.type = 'deposit';
    };
    LoginPage.prototype.segmentChanged = function (ev) {
        console.log('Segment changed', ev);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        //Validate the ip address
        if (!this.dataValidation.isValidIpAddress(this.ipAddress)) {
            this.msgHelper.showErrorDialog('Alert!', 'Invalid Ip Address');
            return;
        }
        //Insert the request into the localstorage
        localStorage.setItem(this.codes.LSK_USERNAME, this.userName);
        localStorage.setItem(this.codes.LSK_PASSWORD, this.password);
        //Create the request json
        var loginRequestJson = {
            "RequestInput": {
                "Authentication": {
                    "username": this.userName,
                    "password": this.password
                },
                "Token": {
                    "tokenId": "123456785"
                }
            }
        };
        var loading = this.msgHelper.showWorkingDialog('Authenticating User');
        localStorage.setItem(this.codes.LSK_IPADDRESS, this.ipAddress);
        this.http.callApi(loginRequestJson, this.codes.API_AUTHENTICATE_USER).then(function (responseJson) {
            loading.dismiss();
            // var json:any = [];
            //json = JSON.parse(responseJson);
            console.log(responseJson['status']);
            if (_this.dataValidation.isEmptyJson(responseJson)) {
                _this.msgHelper.showErrorDialog('Server error', 'Empty response received from back end server.Please try after some time.');
                return;
            }
            if (responseJson['status'] == 1) {
                localStorage.setItem(_this.codes.LSK_USER_INFO_PREFERENCES, JSON.stringify(responseJson['UserDetails']));
                _this.msgHelper.showToast(responseJson['message']);
                _this.navCtrl.setRoot('HomePage');
            }
            else {
                _this.msgHelper.showToast(responseJson['message']);
            }
        }, function (error) {
            loading.dismiss();
            console.error(error);
            _this.msgHelper.showErrorDialog('Server error', error);
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"E:\24Online\Code\RightEpay\src\pages\login\login.html"*/'\n\n\n\n<ion-content padding >\n\n  <p class="small-text pl-20 pr-20">\n\n    <img src="../../assets/imgs/logo.png" style="width: 70px !important;" />\n\n\n\n    <span style="float: right !important;">\n\n      <ion-icon name="notifications" style="margin-top: 10px !important; color: #eee !important;"></ion-icon>\n\n    </span>\n\n  </p>\n\n  <ion-segment color="primary" [(ngModel)]="type" (ionChange)="segmentChanged($event)" scrollable>\n\n    <ion-segment-button value="deposit" checked>\n\n      <ion-label>Login</ion-label>\n\n      <ion-icon name="cash"></ion-icon>\n\n    </ion-segment-button>\n\n    <ion-segment-button value="loan">\n\n      <ion-label>SignUp</ion-label>\n\n      <ion-icon name="log-out"></ion-icon>\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <div [ngSwitch]="type">\n\n    <ion-list *ngSwitchCase="\'deposit\'">\n\n<div class="login-form-details">\n\n      <ion-item style="display: none;">\n\n        <ion-label color="primary" stacked>IP Address</ion-label>\n\n        <ion-input type="tel" placeholder="IP address" class="input-underline" [(ngModel)]="ipAddress">\n\n        </ion-input>\n\n      </ion-item>\n\n\n\n\n\n     <div class="user">\n\n        \n\n        <ion-input type="email" placeholder="Your registered username"  [(ngModel)]="userName">\n\n        </ion-input>\n\n      </div>\n\n\n\n      <div class="user" style="margin-top: 20px;">\n\n        \n\n        <ion-input type="password" placeholder="Your password"  [(ngModel)]="password"></ion-input>\n\n      </div>\n\n\n\n    <p class="small-text pl-20 pr-20" style="margin-top: 20px;">\n\n        <a href="#" class="links">Forget Password?</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" class="links">Other Login issues?</a>\n\n    </p>\n\n    <p style="text-align: center;">\n\n      <button ion-button clear (click)="login()" class="login">Login Securely</button>\n\n    </p>\n\n    <p class="small-text pl-20 pr-20" style="margin-top: 10px;">\n\n      <a href="#" class="links">Terms & Conditions</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#" class="links">Privacy Policy</a>\n\n  </p>\n\n</div>\n\n  </ion-list> \n\n  <ion-list *ngSwitchCase="\'loan\'">\n\n    <div class="login-form-details">\n\n      <div class="user">\n\n        <ion-input type="text" placeholder="Your Name"  [(ngModel)]="name">\n\n        </ion-input>\n\n      </div>\n\n\n\n      <div class="user" style="margin-top: 20px;">\n\n        <ion-input type="text" placeholder="Your Pan No" [(ngModel)]="panno">\n\n\n\n        </ion-input>\n\n      </div>\n\n      <div class="user" style="margin-top: 20px;">\n\n        <ion-input type="text" placeholder="Your Phone No" [(ngModel)]="phone">\n\n        </ion-input>\n\n      </div>\n\n      <div class="user" style="margin-top: 20px;">\n\n        <ion-input type="email" placeholder="Your Email"  [(ngModel)]="email">\n\n        </ion-input>\n\n      </div>\n\n      <div class="user" style="margin-top: 20px;">\n\n        <ion-input type="text" placeholder="Your State"  [(ngModel)]="state">\n\n        </ion-input>\n\n      </div>\n\n      <div class="user" style="margin-top: 20px;">\n\n        <ion-input type="text" placeholder="Your Sponsor"  [(ngModel)]="sponsor">\n\n        </ion-input>\n\n      </div>\n\n      <p style="text-align: center;">\n\n        <button ion-button clear  class="login">Register</button>\n\n      </p>\n\n    </div>\n\n    </ion-list>\n\n </div>\n\n  \n\n  \n\n  <h2 style="line-height: normal;\n\n  font-family: \'MANROPESEMIBOLD\';\n\n  text-align: center;\n\n  margin-top: 26px;\n\n  color: #03b16d;\n\n  font-weight: normal;\n\n  font-size:20px;">Benefits of RightEPay Account</h2>\n\n  <ul class="login-usp">\n\n    <li> <img src="../assets/imgs/fas-secure.png" class="img-responsive" alt="">\n\n      <p>Fast &amp; Secure <span>Payments</span></p>\n\n    </li>\n\n    <li> <img src="../assets/imgs/fas-secure.png" class="img-responsive" alt="">\n\n      <p>Pay Utility Bills or Mobile <span>Recharge &amp; Get Cashbacks</span></p>\n\n    </li>\n\n    <li> <img src="../assets/imgs/fas-secure.png" class="img-responsive" alt="">\n\n      <p>Amazing deals on Travel,<span>Movies &amp; Shopping</span></p>\n\n    </li>\n\n  </ul>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"E:\24Online\Code\RightEpay\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__Utils_DataValidation__["a" /* DataValidation */], __WEBPACK_IMPORTED_MODULE_2__providers_message_helper__["a" /* MessageHelper */],
            __WEBPACK_IMPORTED_MODULE_1__providers_data_data__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_0__Utils_Codes__["a" /* Codes */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=4.js.map