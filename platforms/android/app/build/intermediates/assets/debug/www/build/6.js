webpackJsonp([6],{

/***/ 678:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(689);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



//import { CreateLeadPage } from '../create-lead/create-lead';
var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.openPage = function (pagetitle) {
        this.navCtrl.push('CreateLeadPage', { title: pagetitle });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"E:\24Online\Code\RightEpay\src\pages\home\home.html"*/'<ion-header>\n\n  <div class="headersec">\n\n   \n\n    <button ion-button menuToggle class="burgermenu">\n\n      <img src="../../assets/imgs/burgericon.png">\n\n    </button>\n\n    <div class="logo"><img src="../../assets/imgs/logo.jpg"></div>\n\n    <div class="rytheadsec">\n\n    <div class="notify"><div class="notification"></div><img src="../../assets/imgs/notify.png"></div>\n\n    <div class="walletsec">Main Wallet<span>Rs. 275</span></div>\n\n    </div>\n\n    </div>\n\n</ion-header>\n\n<ion-content padding >\n\n<div>\n\n  \n\n  \n\n  <div class="bodysec">\n\n  \n\n  <div class="bannersec">\n\n    <ion-slides autoplay="3000" loop="true" speed="300" pager="true">\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/slide01.png">\n\n      </ion-slide>\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/slide01.png">\n\n      </ion-slide>\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/slide01.png">\n\n      </ion-slide>\n\n    </ion-slides>\n\n    </div>\n\n  \n\n  <div class="slidetxt"><marquee>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</marquee></div>\n\n  \n\n  <div class="greybg">\n\n  <div class="row">\n\n  <div class="sgleitem"><a href="#"><img src="../../assets/imgs/featured-block-img1.jpg"></a></div>\n\n  <div class="sgleitem"><a href="#"><img src="../../assets/imgs/featured-block-img2.jpg"></a></div>\n\n  <div class="sgleitem"><a href="#"><img src="../../assets/imgs/featured-block-img3.jpg"></a></div>\n\n  <div class="sgleitem"><a href="#"><img src="../../assets/imgs/featured-block-img4.jpg"></a></div>\n\n  </div>\n\n  </div>\n\n  \n\n  <div class="iconsec">\n\n  <h1>Book with RightEPay</h1>\n\n  <div class="row">\n\n  <div class="sgleiconsec" (click)="openPage(\'Modile\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon01.png" alt=""></div>\n\n  <div class="title">Mobile</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'DTH\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon02.png" alt=""></div>\n\n  <div class="title">DTH</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Electricity\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon03.png" alt=""></div>\n\n  <div class="title">Electricity</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'DMT\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon04.png" alt=""></div>\n\n  <div class="title">DMT</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Shop\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon05.png" alt=""></div>\n\n  <div class="title">Shop</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'PAN Card\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon06.png" alt=""></div>\n\n  <div class="title">PAN Card</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Train Ticket\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon07.png" alt=""></div>\n\n  <div class="title">Train Ticket</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Hotel\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon08.png" alt=""></div>\n\n  <div class="title">Hotel</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Insurance\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon09.png" alt=""></div>\n\n  <div class="title">Insurance</div>\n\n  </div>\n\n  <div class="sgleiconsec" (click)="openPage(\'Utility Bill\');">\n\n  <div class="iconwhtebg"><img src="../../assets/imgs/icon09.png" alt=""></div>\n\n  <div class="title">Utility Bill</div>\n\n  </div>\n\n  </div>\n\n  </div>\n\n  \n\n  <div class="graphicsec">\n\n    <ion-slides autoplay="2000" loop="true" speed="200">\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/graphic01.png">\n\n      </ion-slide>\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/graphic01.png">\n\n      </ion-slide>\n\n      <ion-slide>\n\n        <img src="../../assets/imgs/graphic01.png">\n\n      </ion-slide>\n\n    </ion-slides>\n\n    \n\n    </div>\n\n  \n\n  </div>\n\n  \n\n \n\n  \n\n  </div>\n\n</ion-content>\n\n<ion-footer>\n\n  <div class="footersec">\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr01.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr02.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr03.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr04.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr05.jpg"></div>\n\n    </div>\n\n\n\n</ion-footer>'/*ion-inline-end:"E:\24Online\Code\RightEpay\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=6.js.map