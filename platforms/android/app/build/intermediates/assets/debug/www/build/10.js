webpackJsonp([10],{

/***/ 677:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateLeadPageModule", function() { return CreateLeadPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_lead__ = __webpack_require__(688);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CreateLeadPageModule = /** @class */ (function () {
    function CreateLeadPageModule() {
    }
    CreateLeadPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__create_lead__["a" /* CreateLeadPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__create_lead__["a" /* CreateLeadPage */]),
            ],
        })
    ], CreateLeadPageModule);
    return CreateLeadPageModule;
}());

//# sourceMappingURL=create-lead.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateLeadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Utils_DataValidation__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Utils_Codes__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_data_data__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_message_helper__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreateLeadPage = /** @class */ (function () {
    function CreateLeadPage(navCtrl, navParams, msgHelper, http, codes, dataValidation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.msgHelper = msgHelper;
        this.http = http;
        this.codes = codes;
        this.dataValidation = dataValidation;
        this.firstName = null;
        this.lastName = null;
        this.emailId = null;
        this.contactNo = null;
        this.address1 = null;
        this.groupname = null;
        this.zonename = null;
        this.comment = null;
        this.paymentamount = null;
        this.source = null;
        this.preferredCallTime = null;
        this.closureTime = null;
    }
    CreateLeadPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateLeadPage');
        this.title = this.navParams.get("title");
    };
    CreateLeadPage.prototype.gotoPage = function (page) {
        this.navCtrl.setRoot('HomePage');
    };
    CreateLeadPage.prototype.createLead = function () {
        var _this = this;
        //Validation
        //-- Contact number
        if (!this.dataValidation.isValidMobileNumber(this.contactNo)) {
            this.msgHelper.showToast('Invalid mobile number');
            return;
        }
        //-- Email Id
        if (!this.dataValidation.isValidEmailId(this.emailId)) {
            this.msgHelper.showToast('Invalid email id');
            return;
        }
        //Creare the request json
        var requestJson = {
            "firstname": this.firstName,
            "lastname": this.lastName,
            "emailid": this.emailId,
            "contactno": this.contactNo,
            "address1": this.address1,
            "groupname": this.groupname,
            "comment": this.comment,
            "zonename": this.zonename,
            "usertype": "user",
            "paymentamount": this.paymentamount,
            "source": this.source,
            "preferedcalltime": this.preferredCallTime,
            "closertime": this.closureTime
        };
        console.log("Request Json : " + requestJson);
        var loading = this.msgHelper.showWorkingDialog('Creating the lead ...');
        this.http.callApi(requestJson, this.codes.API_CREATE_LEAD).then(function (responseJson) {
            loading.dismiss();
            if (_this.dataValidation.isEmptyJson(responseJson)) {
                _this.msgHelper.showErrorDialog('Error!!', 'Empty response received from server !!!');
                return;
            }
            _this.msgHelper.showToast(responseJson['responsemsg']);
        }, function (error) {
            console.error(error);
            _this.msgHelper.showErrorDialog('Error!!', error);
        });
    };
    CreateLeadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-create-lead',template:/*ion-inline-start:"E:\24Online\Code\RightEpay\src\pages\create-lead\create-lead.html"*/'<ion-header>\n\n  <div class="headersec">\n\n    <ion-navbar>\n\n      <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    <div class="logo"><img src="../../assets/imgs/logo.jpg"></div>\n\n    <div class="rytheadsec">\n\n    <div class="notify"><div class="notification"></div><img src="../../assets/imgs/notify.png"></div>\n\n    <div class="walletsec">Main Wallet<span>Rs. 275</span></div>\n\n    </div>\n\n    </ion-navbar>\n\n    </div>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <div class="bodysec">\n\n\n\n    <div class="headingbutsec">\n\n    <h1>{{title}} Recharge</h1>\n\n    <a href="#" class="rechargestatementbut">Recharge Statement</a>\n\n    </div>\n\n    \n\n    <div class="formgreybg">\n\n    <div class="sgleformsec">\n\n    <div class="label">Mobile Number</div>\n\n    <input type="number" name="text" class="inputfield" placeholder="Enter Mobile Number" />\n\n    </div>\n\n    <div class="sgleformsec">\n\n    <div class="label">Amount</div>\n\n    <input type="number" name="text" class="inputfield" placeholder="Enter Amout" />\n\n    </div>\n\n    <div class="sgleformsec">\n\n    <div class="label">Select Operator</div>\n\n    <select class="selectfield">\n\n    <option selected="selected">Select</option>\n\n    </select>\n\n    </div>\n\n    <div class="sgleformsec">\n\n    <label class="checkboxcontainer">Checkbox\n\n    <input type="checkbox" checked="checked">\n\n    <span class="checkmark"></span></label>\n\n    </div>\n\n    \n\n    <div class="sgleformsec" style="text-align:center;">\n\n    <input type="submit" name="recharge" value="recharge now" class="rechargebut" />\n\n    </div>\n\n    </div>\n\n    \n\n    </div>\n\n    \n\n    \n\n    \n\n\n\n</ion-content>\n\n<ion-footer>\n\n  <div class="footersec">\n\n    <div class="sgleftrsec" ><img src="../../assets/imgs/ftr01.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr02.jpg"></div>\n\n    <div class="sgleftrsec" (click)="gotoPage(\'home\');"><img src="../../assets/imgs/ftr03.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr04.jpg"></div>\n\n    <div class="sgleftrsec"><img src="../../assets/imgs/ftr05.jpg"></div>\n\n    </div>\n\n\n\n</ion-footer>\n\n'/*ion-inline-end:"E:\24Online\Code\RightEpay\src\pages\create-lead\create-lead.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_message_helper__["a" /* MessageHelper */],
            __WEBPACK_IMPORTED_MODULE_2__providers_data_data__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_1__Utils_Codes__["a" /* Codes */], __WEBPACK_IMPORTED_MODULE_0__Utils_DataValidation__["a" /* DataValidation */]])
    ], CreateLeadPage);
    return CreateLeadPage;
}());

//# sourceMappingURL=create-lead.js.map

/***/ })

});
//# sourceMappingURL=10.js.map